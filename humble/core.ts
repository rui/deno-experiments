/**
 * INFO: The Hyper Ultra Minimalistic Blog Light Engine
 */

import { basenameNoExt, globFiles } from "../common/fs.ts";
import {
  copySync,
  ensureFileSync,
  existsSync,
  WalkEntry as FileInfo,
} from "https://deno.land/std/fs/mod.ts";
import { lodash } from "https://raw.githubusercontent.com/ruivieira/deno-lodash/master/mod.ts";

const HOME = Deno.env.get("HOME");

const all_files = globFiles(`${HOME}/notes`, "*");
const allowed_image_extensions = ["png", "jpg", "jpeg", "svg", "gif"];
const all_images = all_files.filter((f) =>
  f.isFile &&
  allowed_image_extensions.includes(
    f.name.substring(f.name.lastIndexOf(".") + 1),
  )
);

function publishable(file: FileInfo): boolean {
  return Deno.readTextFileSync(file.path).split("\n").some((line) =>
    line.startsWith("publish: true")
  );
}

class Page {
  content: string;
  file: FileInfo;
  name: string;

  constructor(file: FileInfo) {
    this.file = file;
    this.content = Deno.readTextFileSync(file.path);
    this.name = basenameNoExt(file.name, "md");
  }
  save(destination: string) {
    Deno.writeTextFileSync(destination, this.content);
  }
}

export enum WikiLinkType {
  Image = "image",
  Text = "text",
  Video = "video",
}
const wikilink_regex = /([!]?)\[\[(.*?)([#].*?)?([|].*?)?\]\]/gm;
const plain_image_regex =
  /!\[(?<alt>.*?)\]\((?<link>.*?)(\s"(?<name>.*?)")?\)/gm;

export class WikiLink {
  link: string;
  constructor(
    readonly name: string,
    readonly anchor: string | undefined,
    link: string,
    readonly type: WikiLinkType,
    readonly original: string,
  ) {
    this.link = link;
  }

  private static getMediaType(match: string): WikiLinkType {
    if (match.startsWith("!")) {
      if (match.includes(".mp4")) return WikiLinkType.Video;
      else return WikiLinkType.Image;
    } else {
      return WikiLinkType.Text;
    }
  }

  public static fromMatches(matches: RegExpMatchArray): WikiLink {
    const type = WikiLink.getMediaType(matches[0]);
    let name = matches[4] == undefined ? matches[2] : matches[4].slice(1);
    const anchor = matches[3] == undefined ? undefined : matches[3].slice(1);
    let link = matches[4] == undefined ? name : matches[2];
    return new WikiLink(name, anchor, link, type, matches[0]);
  }

  public static fromString(content: string): Array<WikiLink> {
    const wikilinks = [...content.matchAll(wikilink_regex)];
    const plain_images = [...content.matchAll(plain_image_regex)];
    return (wikilinks.concat(plain_images)).map(WikiLink.fromMatches);
  }

  toHugo(): string {
    if (WikiLink.isText(this)) {
      const ref = this.anchor != undefined
        ? `${this.link}#${this.anchor}`
        : this.link;
      return `[${this.name}]({{< ref "${ref}" >}})`;
    } else {
      if (WikiLink.isImage(this)) {
        return `{{< figure src="${this.link}" alt="${this.name}" >}}`;
      } else return `{{< video src="${this.link}" >}}`;
    }
  }

  public static isImage(wikilink: WikiLink): boolean {
    return wikilink.type == WikiLinkType.Image;
  }

  public static isText(wikilink: WikiLink): boolean {
    return wikilink.type == WikiLinkType.Text;
  }

  public static isMedia(wikilink: WikiLink): boolean {
    const type = wikilink.type;
    return type == WikiLinkType.Text || type == WikiLinkType.Video;
  }
}

function findCandidate(path: string): FileInfo | undefined {
  return all_images.filter((image) => image.path.endsWith("/" + path)).shift();
}

function removeTrailingDots(path: string): string {
  return path.replace(/^(?:\.\.\/)+/, "");
}

export function build() {
  const pages = all_files.filter((f) => f.isFile && f.name.endsWith(".md"))
    .filter(publishable).map((file) => new Page(file));
  console.log(`Processing ${pages.length} pages.`);
  const wikilinks = pages.map((page) => WikiLink.fromString(page.content));
  const linkMap = lodash.zip(wikilinks, pages).map((values) => {
    const links = values[0];
    const page = values[1];
    return links!.filter(WikiLink.isText).map((
      link,
    ) => [link.link, page!.name]);
  }).flatMap((e) => e).reduce((m, v) => {
    !m.has(v[0]) ? m.set(v[0], [v[1]]) : m.get(v[0])?.push(v[1]);
    return m;
  }, new Map<string, string[]>());

  const absoluteLinks = wikilinks.map((links) => {
    links.map((link) => {
      link.link = removeTrailingDots(link.link);
      if (WikiLink.isImage(link)) {
        const candidate = findCandidate(link.link);
        if (candidate != undefined) {
          link.link = candidate.path.substring(16, candidate.path.length);
        }
      }
      return link;
    });
    return links;
  });

  // copy images
  absoluteLinks.forEach((links) => {
    links.filter(WikiLink.isImage).forEach((link) => {
      const source = `${HOME}/notes/${link.link}`;
      const destination =
        `${HOME}/Sync/code/sites/blog-source-obsidian/static/${link.link}`;
      if (!existsSync(destination)) {
        ensureFileSync(destination);
        copySync(source, destination, { overwrite: true });
      }
    });
  });

  const linkCounts = new Map(
    Array.from(linkMap.keys()).map((k) => [k, lodash.countBy(linkMap.get(k))]),
  );

  lodash.zip(absoluteLinks, pages)
    .map((values) => {
      const links = values[0];
      const page = values[1];
      links!.forEach((link) => {
        page!.content = page!.content.replace(link.original, link.toHugo());
      });
      return page!;
    }).map((page) => {
      const name = page.name;
      let blNames = "backlinks: []";
      let blCounts = "backlinks_count: []";

      if (linkCounts.has(name)) {
        const backlinks = linkCounts.get(name);
        const wrappedKeys = lodash.keys(backlinks).map((n) => `"${n}"`).join(
          ",",
        );
        blNames = `backlinks: [${wrappedKeys}]`;
        blCounts = `backlinks_count: [${lodash.values(backlinks).join(",")}]`;
      }

      const lines = page.content.split("\n");
      lines.splice(1, 0, blNames);
      lines.splice(2, 0, blCounts);
      page.content = lines.join("\n");
      return page;
    }).forEach((page) => {
      const pageName = page.name == "Index"
        ? "_index.md"
        : `posts/${page.name}.md`;
      page.save(
        `${HOME}/Sync/code/sites/blog-source-obsidian/content/${pageName}`,
      );
    });
}
