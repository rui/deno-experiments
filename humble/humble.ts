import { Command } from "npm:commander@9.4.1";
import {build} from "./core.ts"

const program = new Command();

program
  .name("Humble")
  .description("The Hyper Ultra Minimalistic Blog Light Engine")
  .version("0.0.1");

const build_command = new Command("build")
  .description("Build the site")
  .action((_str, _options) => {
    build();
  });

program.addCommand(build_command);

program.parse();
