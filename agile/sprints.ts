/**
 * INFO: tools to manipulate Sprints
 */

import moment from "npm:moment@2.29.4";
import { JsonDB, Config } from 'npm:node-json-db@2.1.3';

const HOME = Deno.env.get("HOME")
export const db = new JsonDB(new Config(`${HOME}/.config/robby/Sprints`, true, false, '/'));

export class Sprint {
  number: number;
  project: string;
  id: string;
  duration: number;
  start: Date;
  end: Date;
  constructor(number: number, project: string, start: Date, duration: number = 3) {
    this.number = number;
    this.project = project.toUpperCase();
    this.id = `${this.project}-${number}`;
    this.duration = duration;
    this.start = start;
    this.end = moment(start).add(duration, 'weeks').toDate();
  }
}

/**
 * Gets all the sprints for a certain project
 * 
 * @param project Project's name
 * @returns A list of Sprints
 */
export async function getAllSprints(project: string) : Promise<Sprint[]> {
    const data = await db.getData(`/${project}`);
    return Object.keys(data).map(key => data[key] as Sprint);
}


